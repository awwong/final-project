My program is called PlayaBook. PlayaBook is ultimately a program that searches and provides
information about current PGA Tour professionals. It utilizes ESPN's current database of
athletes and returns information through a link that opens for the user. It begins by asking
for an input of a player's name. If the user is unsure of the name of the player they are 
searching for, they may enter a letter which would return a list of players whose name begins
with the letter that was inputted. If the user inputs a name that is incorrect or is not in
ESPN's database of athletes, then the program will notify the user by printing "Invalid Name!"
Once a player's name is inputted, the program will automatically open a web browser window or
tab that will be directed to the player's profile on espn.com, which provides all current 
information on the player. The program continues running so that the user may continue to enter
and search for different players.