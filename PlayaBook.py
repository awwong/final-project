import urllib
import urllib2
import json
import webbrowser
import test


class PlayaBook:

    baseurl = 'http://api.espn.com/v1/sports/golf/pga/athletes/'

    apiKey = 'jxv9zrajhn23jmyem6693m27'

    playerDict = dict()

    playerList = []

#playerDict and playerList tests
    #test.testType(playerDict, "dictionary", "testing playerDict type")
    #test.testType(playerList, "list", "testing playerList type")
    
    def __init__(self):
        self.scrapePlayers()
        self.sortPlayers()

    def scrapePlayers(self):
        html = self.getHTML()
        self.formatHTML(html)

    def getHTML(self):
        #Get HTML
        response = urllib2.urlopen('http://espn.go.com/golf/players')
        return response.read()

    def formatHTML(self, html):
        while html != "":
            myTuple = find_between(html,"/id/",'">')
            pStr = myTuple[0]
            end = myTuple[1]
            splitP = pStr.split("/")
            if(splitP[0] == ''):
                break
            self.playerDict[splitP[1]] = splitP[0]
            html= html[end:]

    def processRawInput(self, player):    
        try:
            player = player.replace(" ","-")
            player = player.lower()
            player_request = self.baseurl + self.playerDict[player] + "?" + "apikey=" + self.apiKey
            result = urllib2.urlopen(player_request)
            player_json_str = result.read()
            player_data = json.loads(player_json_str)
            playerLink = player_data['sports'][0]['leagues'][0]['athletes'][0]['links']['web']['athletes']['href']
            webbrowser.open_new(playerLink)
        except KeyError:
            print "Invalid Name!"
            
#ProcessRawInput Tests
        #test.testEqual(player, "tiger-woods", "testing player output for Tiger Woods")
        #test.testEqual(player_request, "http://api.espn.com/v1/sports/golf/pga/athletes/462?apikey=jxv9zrajhn23jmyem6693m27", "testing correct output for player_request for Tiger Woods")
        #test.testType(player_json_str, "string", "testing player_json_str type")
        #test.testType(player_data, "dictionary", "testing player_data type")
        

    def sortPlayers(self):
        players = self.playerDict.keys()
        self.playerList = sorted(players)

#sortPlayers(self) Test
        #test.testType(self.playerList, "list", "testing playerList type")

    def getPlayersByFirstLetter(self, letter):
        thesePlayers = [player for player in self.playerList if player[0] == letter]
        print '\n---------------PLAYERS------------------'
        for player in thesePlayers:
            print player
        print '----------------------------------------\n'

    def run(self):
        #Get RAW INPUT
        player = raw_input("Enter athlete's name: ")
        while(player != "q"):
            if len(player) == 1:
                self.getPlayersByFirstLetter(player[0])
            else:
                self.processRawInput(player)
            player = raw_input("Enter athlete's name: ")

##End of Class

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return (s[start:end], end)
    except ValueError:
        return ("",0)

playaBook = PlayaBook()
playaBook.run()
            





    



